//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use crate::v20201231;

impl From<super::VolumeCreate> for Option<super::VolumeCreateExt> {
    fn from(volume_create: super::VolumeCreate) -> Self {
        Some(super::VolumeCreateExt {
            id: volume_create.id,
            description: volume_create.description,
            name: volume_create.name,
            size_gb: volume_create.size_gb,
            replicas: volume_create.replicas.into_iter().map(Into::into).collect(),
            source: volume_create.source.map(super::VolumeSourceExt::from),
        })
    }
}

impl From<v20201231::VolumeCreate> for Option<super::VolumeCreateExt> {
    fn from(volume_create: v20201231::VolumeCreate) -> Self {
        super::VolumeCreateExt {
            id: volume_create.id,
            name: volume_create.name,
            size_gb: volume_create.size_gb,
            description: volume_create.description,
            replicas: volume_create
                .replicas
                .into_iter()
                .filter_map(Into::into)
                .map(|replica: super::ReplicaCreate| replica.into())
                .collect(),
            source: volume_create.source.map(Into::into),
        }
        .into()
    }
}

impl From<v20201231::ReplicaCreate> for Option<super::ReplicaCreate> {
    fn from(replica_create: v20201231::ReplicaCreate) -> Self {
        Some(super::ReplicaCreate {
            name: replica_create.name,
            description: replica_create.description,
            realm: replica_create.realm,
            primary: replica_create.primary,
        })
    }
}

impl From<super::Volume> for v20201231::Volume {
    fn from(volume_create: super::Volume) -> Self {
        Self {
            name: volume_create.name,
            id: volume_create.id,
            description: volume_create.description,
            size_gb: volume_create.size_gb,
            source: volume_create.source.and_then(Into::into),
            replicas: volume_create.replicas.into_iter().map(Into::into).collect(),
            iqn: volume_create.iqn,
            job: volume_create.job,
            created: volume_create.created,
            modified: volume_create.modified,
            status: volume_create.status,
        }
    }
}

impl From<super::Replica> for v20201231::Replica {
    fn from(replica: super::Replica) -> Self {
        Self {
            name: replica.name,
            description: replica.description,
            realm: replica.realm.into(),
            primary: replica.primary,
            iscsi_portal: replica.iscsi_portal,
            status: replica.status,
        }
    }
}

impl From<super::ReplicaRealm> for v20201231::ReplicaRealm {
    fn from(replica_realm: super::ReplicaRealm) -> Self {
        match replica_realm {
            super::ReplicaRealm::Aws {
                realm,
                region,
                subnet,
            } => Self::Aws {
                realm,
                region,
                subnet,
            },
            super::ReplicaRealm::Azure {
                realm,
                resource_group,
                subnet,
                ..
            } => Self::Azure {
                realm,
                resource_group,
                subnet,
            },
        }
    }
}

impl From<v20201231::VolumeSource> for super::VolumeSourceExt {
    fn from(volume_source: v20201231::VolumeSource) -> Self {
        match volume_source {
            v20201231::VolumeSource::AwsSnapshot(snapshot) => Self::AwsSnapshot(snapshot),
        }
    }
}

impl From<super::VolumeSource> for Option<v20201231::VolumeSource> {
    fn from(volume_source: super::VolumeSource) -> Self {
        match volume_source {
            super::VolumeSource::AwsSnapshot(snapshot) => {
                Some(v20201231::VolumeSource::AwsSnapshot(snapshot))
            }
            _ => None,
        }
    }
}

impl From<super::WsEvent> for v20201231::WsEvent {
    fn from(event: super::WsEvent) -> Self {
        match event {
            super::WsEvent::Job(job) => Self::Job(job),
            super::WsEvent::Volume(volume) => Self::Volume(volume.into()),
        }
    }
}
