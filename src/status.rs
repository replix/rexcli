//
// Copyright (c) 2020 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

pub(crate) fn volume(status: &str) -> Result<String, anyhow::Error> {
    match status {
        s @ "uninitialized"
        | s @ "provisioning"
        | s @ "starting"
        | s @ "stopping"
        | s @ "reconfiguring"
        | s @ "online"
        | s @ "offline"
        | s @ "syncing"
        | s @ "degraded"
        | s @ "error" => Ok(s.to_string()),
        s => Err(anyhow::format_err!("No such status '{}'", s)),
    }
}

pub(crate) fn job(status: &str) -> Result<String, anyhow::Error> {
    match status {
        s @ "pending" | s @ "in_progress" | s @ "done" | s @ "error" => Ok(s.to_string()),
        s => Err(anyhow::format_err!("No such job status: `{}`", s)),
    }
}
