//
// Copyright (c) 2020 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::fmt;
use std::str;

use crate::v20201231;
use crate::v20210228;

#[derive(Clone, Copy, Debug, PartialEq)]
pub(crate) enum ApiEndpoint {
    V20201231,
    V20210228,
    Vnext,
}

impl str::FromStr for ApiEndpoint {
    type Err = String;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "v20201231" | "V20201231" | "2020-12-31" | "20201231" | "1231" | "12" => {
                Ok(Self::V20201231)
            }
            "v20210228" | "V20210228" | "2021-02-28" | "20210228" | "0228" | "02" => {
                Ok(Self::V20210228)
            }
            "vnext" | "VNEXT" | "Vnext" => Ok(Self::Vnext),
            _ => Err(String::from("Unkown API Endpoint")),
        }
    }
}

impl ApiEndpoint {
    pub(crate) fn endpoint(&self) -> &str {
        match self {
            Self::V20201231 => v20201231::VERSION,
            Self::V20210228 => v20210228::VERSION,
            Self::Vnext => "vnext",
        }
    }
}

impl fmt::Display for ApiEndpoint {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let text = match self {
            Self::V20201231 => "2020-12-31",
            Self::V20210228 => "2021-02-28",
            Self::Vnext => "vnext",
        };
        text.fmt(f)
    }
}

impl Default for ApiEndpoint {
    fn default() -> Self {
        Self::V20210228
    }
}
