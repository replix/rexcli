//
// Copyright (c) 2020 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::time::Duration;

use crate::latest;

pub(crate) fn stepping_down_delay(initial: Duration) -> BackOn {
    BackOn::new(initial)
}

pub(crate) struct BackOn {
    next: Duration,
    min: Duration,
}

impl BackOn {
    pub(crate) fn new(initial: Duration) -> Self {
        let next = initial;
        let min = Duration::from_secs_f32(1.5);
        Self { next, min }
    }
}

impl Iterator for BackOn {
    type Item = Duration;
    fn next(&mut self) -> Option<Self::Item> {
        let next = self.next;
        let millis = 500 + rand::random::<u64>() % 1000;
        let step = Duration::from_millis(millis);
        self.next = std::cmp::max(self.next - step, self.min);
        Some(next)
    }
}

impl crate::RealmSpec {
    pub(crate) fn to_api(&self) -> latest::RealmTypeCreate {
        match self.clone() {
            Self::Aws {
                role_arn,
                external_id,
            } => latest::RealmTypeCreate::Aws {
                r#type: "aws".to_owned(),
                role_arn,
                external_id,
            },
            Self::Azure {
                subscription_id,
                tenant_id,
                client_id,
                client_secret,
            } => latest::RealmTypeCreate::Azure {
                r#type: "azure".to_owned(),
                subscription_id,
                tenant_id,
                client_id,
                client_secret,
            },
        }
    }
}

impl latest::SnapshotExportCreate {
    pub(crate) fn aws(realm: impl AsRef<str>, region: impl AsRef<str>) -> Self {
        let realm = realm.as_ref().to_string();
        let region = region.as_ref().to_string();
        let aws = latest::AwsSnapshotExportCreate { realm, region };
        Self::Aws(aws)
    }
}

impl latest::SnapshotExport {
    pub(crate) fn aws_snapshot_id(&self) -> Option<String> {
        None
    }
}
