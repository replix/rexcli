//
// Copyright (c) 2020 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::fmt;

use crate::latest;

impl fmt::Display for latest::Status {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let status = f.write_fmt(format_args!("Status: {}", self.value));
        if let Some(ref message) = self.message {
            f.write_fmt(format_args!(" ({})", message))
        } else {
            status
        }
    }
}

impl fmt::Display for latest::Replica {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("Replica name  : {}", self.name))
            .and_then(|_| f.write_fmt(format_args!("Replica status: {}", self.status)))
    }
}

impl fmt::Display for latest::Snapshot {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("Snapshot name: {}", self.name))
            .and_then(|_| f.write_fmt(format_args!("Snapshot id: {}", self.id)))
    }
}

impl fmt::Display for latest::SnapshotExport {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("Export id: {}", self.id))
            .and_then(|_| f.write_fmt(format_args!("Export status : {}", self.status)))
    }
}

impl fmt::Display for latest::Realm {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("Realm           : \"{}\"\n", self.name))
            .and_then(|_| f.write_fmt(format_args!("Created         : {}\n", self.created)))
            .and_then(|_| f.write_fmt(format_args!("{}", self.realm)))
    }
}

impl fmt::Display for latest::RealmType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Aws { role_arn } => {
                f.write_fmt(format_args!("Type            : AWS\nRole ARN        : {}", role_arn))
            }
            Self::Azure {
                subscription_id,
                tenant_id,
                client_id,
            } => f.write_fmt(format_args!(
                "Type            : Azure\nSubscription id : {}\nTenant id       : {}\nClient id       : {}",
                subscription_id, tenant_id, client_id
            )),
        }
    }
}
