//
// Copyright (c) 2020 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use serde_json as json;

use crate::latest;

pub(crate) trait RexError: Sized {
    fn rexerror_for_status(self) -> Result<Self, anyhow::Error>;
}

impl RexError for attohttpc::Response {
    fn rexerror_for_status(self) -> Result<Self, anyhow::Error> {
        if self.is_success() {
            Ok(self)
        } else {
            let body = self.text()?;
            if let Ok(error) = json::from_str::<latest::Error>(&body) {
                Err(anyhow::anyhow!(error.message))
            } else {
                Err(anyhow::anyhow!("Raw HTTP Response:\n{}", body))
            }
        }
    }
}
