//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use serde_json as json;

use crate::v20201231::*;

const REALMS: &str = r#"[
    {
        "name": "azure",
        "created": "2021-02-21T15:06:42.750Z",
        "azure": {
            "client_id": "uuid",
            "tenant_id": "uuid",
            "subscription_id": "uuid"
        }
    },
    {
        "name": "aws",
        "created": "2021-02-21T15:06:42.685Z",
        "aws": {
            "role_arn": "arn:aws:iam::......SA3YYB64AE9U"
        }
    }
]"#;

#[test]
fn realms() {
    let realms = json::from_str::<Vec<Realm>>(REALMS).unwrap();
    assert_eq!(realms.len(), 2);

    let azure = &realms[0];
    let aws = &realms[1];

    assert_eq!(azure.name, "azure");
    assert!(matches!(azure.realm, RealmType::Azure { .. }));

    assert_eq!(aws.name, "aws");
    assert!(matches!(aws.realm, RealmType::Aws { .. }));
}
