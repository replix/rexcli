build:
    cargo build --workspace --all-targets
clean:
    cargo clean
test:
    cargo test --all-targets

clippy:
    cargo clippy --workspace --all-targets
c:
    cargo c
pedantic:
    cargo clippy --workspace --all-targets --features pedantic
update:
    cargo update
bloat:
    cargo bloat
cbuild: clean build
ctest: clean test
rustfmt:
    cargo fmt --all -- --check
alias fmt := rustfmt
check: rustfmt update test clippy
fixlock:
    rm Cargo.lock
    cargo update
    git add Cargo.lock
